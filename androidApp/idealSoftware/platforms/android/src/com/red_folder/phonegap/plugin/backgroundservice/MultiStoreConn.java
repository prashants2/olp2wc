package com.red_folder.phonegap.plugin.backgroundservice;

/**
 * Created by seemag on 20/12/2017.
 */
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import android.graphics.Color;

import net.idealss.vr.castle.MainActivity;
import net.idealss.vr.castle.R;

import net.idealss.api.BayeuxNotificationHandler;
import net.idealss.api.BayeuxSubscriptionHandler;
import net.idealss.api.IdealAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;

import static android.content.Context.NOTIFICATION_SERVICE;

public class MultiStoreConn implements BayeuxNotificationHandler, BayeuxSubscriptionHandler {

    IdealAPI idealAPI;
    private BayeuxSubscriptionHandler bayeuxSubscriptionHandler;
    private String responseChannel;
    public boolean isSubscribed = false;
    private String KEY_REPLY_HISTORY = "notification_key";
    private int NOTIFICATION_ID = 12345;
    private String KEY_REPLY = "reply_key";
    private String replyMsg = "";
    private String VToken;
    private String responseChannelPrefix;
    private String responseChannelPrefixNew;
    NotificationManager notificationManager;
    public String JsonResult = "";
    public JSONObject JsonMsg;
    public boolean SubscribedReuest = false;
    private String msgReq = "";
    private String msgResp = "";
    private String dateTime = "";
    private String token = "";
    private int storeID;
    private String apiName = "";
    private int groupId;
    private String privateKey = "";
    private String apiToken = "";
    private String clientUrl = "";
    private String clientID;
    private String emailID = "";
    private String phoneNumber = "";
    private String deviceID = "";
    private String devicePlatform = "";
    private JSONObject data;
    private Context mContext;
    DatabaseHandler db;
    MesaageListener mesaageListener;

    public MultiStoreConn(Context context, int siteID, String api, int group, String Key, String Token, String client_Url, String email, String phone, String device, String devicePlatf, MesaageListener mesaageListener) {

        try {
            this.mesaageListener = mesaageListener;
            db = new DatabaseHandler(context);
            mContext = context;
            storeID = siteID;
            groupId = group;
            privateKey = Key;
            apiToken = Token;
            apiName = api;
            clientUrl = client_Url;
            emailID = email;
            deviceID = device;
            devicePlatform = devicePlatf;
            phoneNumber = phone;
            bayeuxSubscriptionHandler = this;
            idealAPI = new IdealAPI(clientUrl, false, this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setConnection() {
        try {
            idealAPI.setAPI(apiName);
            idealAPI.setGroup(groupId);
            idealAPI.setLocationId(storeID);
            idealAPI.setPrivateKey(privateKey);
            idealAPI.setToken(apiToken);
            responseChannelPrefix = "/" + idealAPI.getAPI() + "/" + idealAPI.getGroup() + "/" + idealAPI.getLocationId();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onHandshakeComplete() {
        Log.e("onHandshakeComplete....", "onHandshakeComplete");

        try {
            setConnection();
            responseChannel = "/" + idealAPI.getAPI() + "/" + idealAPI.getGroup() + "/" + idealAPI.getClientID() + "/response";

            if (!isSubscribed) {
                idealAPI.subscribe(responseChannel, bayeuxSubscriptionHandler);
                isSubscribed = true;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onUnsuccessful(JSONObject jsonObject) {
        Log.e("onUnsuccessful.........", jsonObject.toString());
    }

    @Override
    public void onClientIDChanged(String s, String s1) {
        Log.e("onClientIDChanged....", s + "" + s1);
    }

    @Override
    public void onLogMessage(JSONObject jsonObject) {
        Log.e("onLogMessage.....", jsonObject.toString());
    }

    @Override
    public void onLogResponse(JSONObject jsonObject) {
        Log.e("onLogResponse.......", jsonObject.toString());
        // setConnection();

        try {

            if (jsonObject.getString("channel").equals("/meta/subscribe")) {
                Log.e("/meta/subscribe.......", "working" + storeID);
            }
            if (jsonObject.getString("channel").equals("/meta/subscribe")) {
                if (!SubscribedReuest) {
                    customer_helloReq();
                    Log.e("subscribe here", "subscribe" + storeID);
                    SubscribedReuest = true;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLogVerbose(String s) {
        Log.e("onLogVerbose......", s);
    }

    @Override
    public Authentication onAuthenticationNeeded() {
        return null;
    }

    @Override
    public void onMessageReceived(JSONObject jsonObject) {
        Log.e("onMessageReceived", "complete" + jsonObject.toString());

        try {
            if (jsonObject.getJSONObject("data").getString("message").equals("customer_message_ack")) {
                String val = (jsonObject.getJSONObject("data").getJSONObject("v1").getString("success"));
                String currentTimeStamp = (jsonObject.getJSONObject("data").getJSONObject("v1").getString("currentTimeStamp"));

                if (val == "true") {
                    dateTime = currentTimeStamp;
                    msgResp = "";
                    //db.addContact(new Notifications(deviceID,msgReq,msgResp,dateTime));
                }
            }

            if (jsonObject.getJSONObject("data").getString("message").equals("customer_hello")) {
                data = jsonObject.getJSONObject("data");
                JsonResult = jsonObject.getJSONObject("data").getJSONObject("v1").getString("token");
                VToken = jsonObject.getJSONObject("data").getJSONObject("v1").getString("token");
                int aa = Integer.parseInt(jsonObject.getString("api_location"));
                //JsonResult=  jsonObject.getJSONObject("data").getJSONObject("v1").getString("messageBody");
                String currentTimeStamp = toISO8601Date(new Date());
                msgResp = "";
                dateTime = currentTimeStamp;
                msgReq = "";
                token = JsonResult;
                storeID = idealAPI.getLocationId();
                clientID = idealAPI.getClientID();

                db.addContact(new Notifications(deviceID, msgReq, msgResp, dateTime, token, storeID, clientID));

                // createInlineNotification(mContext,"Test", JsonResult,aa);
                //runOnce();
                //doWork();
                mesaageListener.onMesasgeRecived(jsonObject);


            } else if (jsonObject.getJSONObject("data").getString("message").equals("customer_message")) {

                data = jsonObject.getJSONObject("data");
                JsonResult = jsonObject.getJSONObject("data").getJSONObject("v1").getString("messageBody");
                int aa = Integer.parseInt(jsonObject.getString("api_location"));

                String currentTimeStamp = toISO8601Date(new Date());
                msgResp = JsonResult;
                //  dateTime = currentTimeStamp;
                msgReq = "";
                token = "";
                storeID = idealAPI.getLocationId();
                clientID = idealAPI.getClientID();

                db.addContact(new Notifications(deviceID, msgReq, msgResp, currentTimeStamp, token, storeID, clientID));

                // doWork();
                // resp(JsonResult);
                createInlineNotification(mContext, "", JsonResult, aa);
//                createInlineNotification(mContext, "Test", JsonResult, aa);
                //runOnce();
                mesaageListener.onMesasgeRecived(jsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void customer_helloReq() {

        String data = customerHelloJson();
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(data);
            idealAPI.publish(responseChannelPrefix, jsonObj);
            // responseChannelPrefixNew=  "/" + api.getAPI() + "/" + api.getGroup() + "/" + 456;
            //  api.subscribe(responseChannel, bayeuxSubscriptionHandler);
            // api.publish(responseChannelPrefixNew, jsonObj);

            Log.e("customer_hello_request", jsonObj.toString());

        } catch (JSONException e) {
            e.printStackTrace();

        }
    }

    private String customerHelloJson() {
        //   863194038647266
        deviceID = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);

        //  String m_androidId = "863194038647266";

        JSONObject jsonObj = new JSONObject();
        JSONObject json = new JSONObject();
        try {
            jsonObj.put("returnChannel", responseChannel);
            jsonObj.put("messageID", getGuid());

            jsonObj.put("siteID", idealAPI.getLocationId());
            jsonObj.put("token", "");


            jsonObj.put("deviceID", deviceID);
            jsonObj.put("platform", devicePlatform);

            jsonObj.put("phone", phoneNumber);
            jsonObj.put("email", emailID);
            jsonObj.put("currentTimeStamp", toISO8601Date(new Date()));

            json.put("message", "customer_hello");

            json.put("v1", jsonObj);

            return json.toString();

        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String toISO8601Date(Date date) {
        try {
            Log.e("current date.......", date.toString());
//            TimeZone tz = TimeZone.getTimeZone("UTC");
            TimeZone tz = TimeZone.getDefault();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss");
            df.setTimeZone(tz);
            return df.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String getGuid() {
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        String messageid = randomUUIDString;
        return messageid;
    }

    public void customer_messageReq(String msg) {
        String data = customer_message(VToken, msg);
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(data);
            Log.e("customer_message", data);
            Log.e("customer_message", jsonObj.toString());
            idealAPI.publish(responseChannelPrefix, jsonObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String customer_message(String token, String msg) {
        JSONObject jsonObj = new JSONObject();
        JSONObject json = new JSONObject();
        try {

            jsonObj.put("messageID", getGuid());
            jsonObj.put("siteID", idealAPI.getLocationId());
            jsonObj.put("token", token);
            jsonObj.put("ackChannel", "");

            jsonObj.put("messageBody", msg);
            jsonObj.put("currentTimeStamp", toISO8601Date(new Date()));
            json.put("message", "customer_message");
            json.put("v1", jsonObj);

            Log.e("customer_messageRequest", token);
            return json.toString();

        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    // notification..
    private void createInlineNotification(Context context, String title, String message, int siteID) {

        try {

            String NOTIFICATION_CHANNEL_ID = "10001";
            //create notification when OS 8.0 or above and notificationChannel 06-20-19
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                String description = "channel_description";
                NotificationChannel channel = new  NotificationChannel(NOTIFICATION_CHANNEL_ID, "VersirentNotifications", NotificationManager.IMPORTANCE_DEFAULT);
                channel.setDescription(description);
                NotificationManager manager = mContext.getSystemService(NotificationManager.class);
                manager.createNotificationChannel(channel);
            }
            //End 06-20-19

            //Create notification builder
//            NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
            NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(android.R.drawable.stat_notify_chat)
    //          .setContentTitle(title + "--" + message)
                .setContentTitle(title + message)
                .setDefaults(Notification.DEFAULT_ALL).
                 setPriority(Notification.PRIORITY_HIGH);

            Intent notificationIntent = new Intent(context, MainActivity.class);
            notificationIntent.putExtra("fromNotification", "message");
            notificationIntent.putExtra("myMSg", siteID);
            PendingIntent contentIntent = PendingIntent.getActivity(context,
                    0, notificationIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            notificationIntent.setAction(Intent.ACTION_MAIN);
            notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            builder.setContentIntent(contentIntent);

            String replyLabel = "Enter your reply here";
            //Initialise RemoteInput
            android.support.v4.app.RemoteInput remoteInput = new android.support.v4.app.RemoteInput.Builder(KEY_REPLY)
                    .setLabel(replyLabel)
                    .build();

            int randomRequestCode = new Random().nextInt(54325);

            //PendingIntent that restarts the current activity instance.
            Intent resultIntent = new Intent(context, MyService.class);
            resultIntent.putExtra("fromNotification", "message");
            //  resultIntent.putExtra("myMSg",siteID);

            resultIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //Set a unique request code for this pending intent

            PendingIntent resultPendingIntent = PendingIntent.getService(context, randomRequestCode, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            // resultPendingIntent.putExtra("myMSg",siteID);

            //Notification Action with RemoteInput instance added.
            NotificationCompat.Action replyAction = new NotificationCompat.Action.Builder(
                    android.R.drawable.sym_action_chat, "REPLY", resultPendingIntent)
                    .addRemoteInput(remoteInput)
                    .setAllowGeneratedReplies(true)
                    .build();

            //Notification.Action instance added to Notification Builder.
            builder.addAction(replyAction);

            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra("notificationId", NOTIFICATION_ID);
            intent.putExtra("messsageData", JsonResult);

            //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            //  PendingIntent dismissIntent = PendingIntent.getActivity(getBaseContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

            // builder.addAction(android.R.drawable.ic_menu_close_clear_cancel, "DISMISS", dismissIntent);


            //Create Notification.
//            notificationManager =
//                    (NotificationManager)
//                            context.getSystemService(NOTIFICATION_SERVICE);
//
//            notificationManager.notify(NOTIFICATION_ID, builder.build());

            //start 06-20-19
            NotificationManagerCompat manager = NotificationManagerCompat.from(this.mContext);
            manager.notify(NOTIFICATION_ID, builder.build());
            //End 06-20-19

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
