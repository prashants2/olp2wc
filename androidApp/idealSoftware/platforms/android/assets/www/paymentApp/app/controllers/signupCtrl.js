﻿angular.module("app")
    .controller("signCtrl", ["$scope", "$rootScope", "idealsApi", "$location", "$http","configDetailsProvider", function($scope, $rootScope, idealsApi, $location, $http,configDetailsProvider) {

        $scope.accountNo = "";
        $scope.customerNo = "";
        $scope.userName = "";
        $scope.password = "";
        $scope.emailAdd = "";

        var tac_url = "";
        try {
            captcha();
            // terms and conditions
            tac_url = configDetailsProvider.apiConnect[0].tac_url + "/" + configDetailsProvider.apiConnect[0].location_url + "/tac.txt";
            console.log(tac_url);
            if (tac_url != "") {
                $scope.termsCondtions = "";
                $http.get(tac_url).success(function(data, status, headers, config) {
                    $scope.termsCondtions = data;
                }).error(function(data, status, headers, config) {
                    $scope.termsCondtions = "";
                });
            } else {
                $scope.termsCondtions = "";
            }

            // function to submit the form after all validation has occurred
            $scope.submit = function() {
                // check to make sure the form is completely valid
                if ($scope.signupForm.$valid) {
                    var isValidCapcha = validCaptcha();
                    if (isValidCapcha) {
                        $rootScope.ShowLoader = true;
                        $scope.Name = "";
                        var signup = {
                            AccountNo: $scope.accountNo,
                            CustomerNo: $scope.customerNo,
                            Username: $scope.userName,
                            Password: $scope.password,
                            Email: $scope.emailAdd
                        };
                        if (signup.AccountNo != "" && signup.CustomerNo != "" && signup.Username != "" && signup.Password != "" && signup.Email != "") {
                            idealsApi.signupReq(signup.AccountNo, signup.CustomerNo, signup.Username, signup.Password, signup.Email);
                            var data = "";
                        }
                    } else {
                        $rootScope.ShowLoader = false;
                        $scope.CodeNumberTextbox = "";
                        $scope.password = "";
                        $scope.confirmPass = "";
                        captcha();
                        $location.path("/signUp");
                    }
                }
            };

            //Captcha Code
            function captcha() {
                var alpha = new Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
                var i;
                for (i = 0; i < 6; i++) {
                    var a = alpha[Math.floor(Math.random() * alpha.length)];
                    var b = alpha[Math.floor(Math.random() * alpha.length)];
                    var c = alpha[Math.floor(Math.random() * alpha.length)];
                    var d = alpha[Math.floor(Math.random() * alpha.length)];
                    var e = alpha[Math.floor(Math.random() * alpha.length)];
                    var f = alpha[Math.floor(Math.random() * alpha.length)];
                    var g = alpha[Math.floor(Math.random() * alpha.length)];
                }
                var code = a + " " + b + " " + " " + c + " " + d + " " + e + " " + f + " " + g;
                $scope.capchaVal = code;
            };

            $scope.refresh = function() {
                //Captcha Code
                var alpha = new Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
                var i;
                for (i = 0; i < 6; i++) {
                    var a = alpha[Math.floor(Math.random() * alpha.length)];
                    var b = alpha[Math.floor(Math.random() * alpha.length)];
                    var c = alpha[Math.floor(Math.random() * alpha.length)];
                    var d = alpha[Math.floor(Math.random() * alpha.length)];
                    var e = alpha[Math.floor(Math.random() * alpha.length)];
                    var f = alpha[Math.floor(Math.random() * alpha.length)];
                    var g = alpha[Math.floor(Math.random() * alpha.length)];
                }
                var code = a + " " + b + " " + " " + c + " " + d + " " + e + " " + f + " " + g;
                $scope.capchaVal = code;
            };

            function validCaptcha() {
                var string1 = removeSpaces($scope.capchaVal);
                var string2 = removeSpaces($scope.CodeNumberTextbox);
                if (string1 == string2) {
                    return true;
                } else {
                    return false;
                }
            };

            function removeSpaces(string) {
                return string.split(" ").join("");
            };

            document.addEventListener("deviceready", onDeviceReady, false);

            function onDeviceReady() {

                $scope.speak = function() {
                    var text = $scope.capchaVal;
                    TTS.speak({
                        text: text,
                        locale: "en-US",
                        rate: 0.05
                    }, function() {

                    }, function(reason) {

                    });

                }
            }
        } catch (e) {
            console.log(e);
        }

    }]);