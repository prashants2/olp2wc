﻿
angular.module("app")
.directive("allowPattern", function ($rootScope) {
    return {
        restrict: "A",
        compile: function (tElement, tAttrs) {
            return function (scope, element, attrs) {
                // I handle key events
                element.bind("keypress", function (event) {
                    var keyCode = event.which || event.keyCode; // I safely get the keyCode pressed from the event.
                    var keyCodeChar = String.fromCharCode(keyCode); // I determine the char from the keyCode.
                    // If the keyCode char does not match the allowed Regex Pattern, then don't allow the input into the field.
                    if (!keyCodeChar.match(new RegExp(attrs.allowPattern, "i"))) {
                        event.preventDefault();
                        return false;
                    }

                });
            };
        }
    };
});





//<body ng-app="CodePenApp">
//  <h1>AngularJS Directive Labs</h1>
//  <h3>allowPattern directive</h3>

//ALPHA ONLY <input type="text" allow-pattern="[a-z]" />
//<br>
//NUMBER ONLY <input type="text" allow-pattern="\d" />
//<br>
//ALPHANUMERIC ONLY <input type="text" allow-pattern="(\d|[a-z])" />
//<br>
//WHITESPACE CHARACTERS ONLY <input type="text" allow-pattern="\W" />
//<br>
//ABCDEFG ONLY <input type="text" allow-pattern="[ABCDEFG]" />
//</body>