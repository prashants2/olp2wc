﻿// call all api methods using faye server
angular.module('app')
 .service("idealsApi", ['$rootScope', '$cookieStore', 'Apicookies', '$location', '$timeout', function ($rootScope, $cookieStore, Apicookies, $state, $timeout) {
    
     var responseChannelPrefix;
     var clientID;
     var data;
     var evl;
     var client;
     var responseChannel;
     var isSubscribed = false;
     var handshake = '/meta/handshake';
     var connect = '/meta/connect';
     var subscribe = '/meta/subscribe';
     var validToken;
     var Logger;
     var time_last;
     var AuthenticateRequest;
     var apiToken;
     var apiKey;
     var apiStore;
     var groupId;
     var location_url;
     var firstConnection = true;
     var timeout;
     var siteId;
     var waitTime = 120000;
     var waitTimeReq = "";

     try {
         
         // cal and read api_cookies 
         Apicookies.writeApiCookie();  
         groupId = ($cookieStore.get('api_connect')).group;
         apiStore = ($cookieStore.get('api_connect')).api;
         siteId = ($cookieStore.get('api_connect')).location_id;
         responseChannelPrefix = "/" + apiStore + "/" + groupId + "/" + siteId;
      

         // Connect To Faye
         var client_url = ($cookieStore.get('api_connect')).client_url;
         var start = new Date().getTime();
         client = new Faye.Client(client_url, { timeout: 120 });
         client.connect();
         client.disable('WebSocket');

         Logger = {
             incoming: function (message, callback) {
                 console.log(' incoming: ', message);
                 if (message.channel == handshake && message.successful) {
                     obj = JSON.parse(JSON.stringify(message));
                     clientID = (obj.clientId);
                     responseChannel = "/" + apiStore + "/" + groupId + "/" + clientID + "/response";
                     $rootScope.messageReceived = { data: { message: 'connect' } };
                     $rootScope.$apply();
                     if (!isSubscribed) {
                         SubscribeIt(responseChannel);
                     }
                 }
                 return callback(message);
             },
             outgoing: function (message, callback) {
                
                 if (message) {
                  
                     console.log(' outgoing: ', message);

                     try {
                         evl = "";
                         var jsonString = JSON.stringify(message);
                         var json = message;
                         apiToken = ($cookieStore.get('api_connect')).token;
                         apiStore = ($cookieStore.get('api_connect')).api;
                         if (message.channel == subscribe) {

                             var salt = random128();
                             evl = {
                                 "api": apiStore,
                                 "token": apiToken,
                                 "salt": Base64.encode(salt),
                                 "signature": createSignature(salt, jsonString),
                                 "message": subscribe,
                                 "data": Base64.encode(jsonString)
                             };
                             message.ext = evl;
                             return callback(message)
                         };

                         if (json.data != 'undefined' && json.data != null) {
                             var salt = random128();
                             var message1;

                             if ((json.data != null) && (json.data.v1 != null)) {
                                 message1 = json.data.message;
                             }
                             else {
                                 console.log("[VR][API]Packet missing v1 section or v1.message element. \r\n" + json.data);
                             }
                            

                             evl = {
                                 "api": apiStore,
                                 "token": apiToken,
                                 "salt": Base64.encode(salt),
                                 "signature": createSignature(salt, jsonString),
                                 "message": message1,
                                 "data": Base64.encode(jsonString)
                             };


                             message.ext = evl;
                             message.data = json.data;
                         };
                     }
                     catch (err) {
                         console.log(err.message);
                     }
                 }

                 callback(message);
             }
         }


         // call extension 
         client.addExtension(Logger);

         function cancelTimer() {
             console.log('cancel timeout');
             $timeout.cancel(timeout);
         };

         function stopLoader() {
             $rootScope.ShowLoader = false;
             console.log("The server cannot or will not process the request");
             $rootScope.apiErrorMessage = "The server cannot or will not process the request";
         };

         function startTimer() {
             timeout = $timeout(function () {
                 console.log('finish timeout');
                 $rootScope.ShowLoader = false;
                 console.log("The server cannot or will not process the request");
                 $rootScope.apiErrorMessage = "The server cannot or will not process the request";

             }, waitTime);
         };

         //Subscribe here
         function SubscribeIt(rc) {
            
             var subscription = client.subscribe(rc, function (msg) {
                 console.log('subscribed data ' + JSON.stringify(msg));
                 $rootScope.messageReceived = msg;
                 $rootScope.$apply();

                 //clear timer
                 if (msg.data.message == waitTimeReq){
                     cancelTimer();
                 }

                 //check store_information
                 if (sessionStorage.checkStoreInformation == null) {
                     site_informationReq();
                 }
              
             }).then(function (msg) {
                 console.log('subscription successful!!');
                 console.log('subscribed');
                 isSubscribed = true;
                 site_informationReq();
                 list_categoriesReq();
               
             }, function (error) {
                 console.log('Error subscribing: ' + error.message);
             });
         };

         this.disconnect = function () {
             client.unsubscribe();
             client.disconnect();
         }

   
         // signature
         function createSignature(salt, json) {
            
             // api_key
             var api_key = ($cookieStore.get('api_connect')).key;
             var signature = (SHA256(SHA256(SHA256(utf8.encode(json)) + utf8.encode(api_key)) + (salt)));
             return signature;
         };


         // guid
         function CreateGuid() {

             function _p8(s) {
                 var p = (Math.random().toString(16) + "000000000").substr(2, 8);
                 return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
             }
             return _p8() + _p8(true) + _p8(true) + _p8();
         };

         // currentdatetime
         function CreateDate() {
             var currentdate = new Date();
             var currentDateTime = currentdate.toISOString();
             //var currentDateTime = (currentdate.toLocaleString()).replace(',', '');
             return currentDateTime;
         };

         function site_informationReq() {
             var data = site_information();
             var publication = client.publish(responseChannelPrefix, data).then(function () {
                 console.log('Successfully published!');
             }, function (error) {
                 console.log('Error publishing: ' + error.message);

             });
         };

       function list_categoriesReq() {
             var data = list_categories();
             var publication = client.publish(responseChannelPrefix, data).then(function () {
                 console.log('Successfully published!');
             }, function (error) {
                 console.log('Error publishing: ' + error.message);

             });
         };


         //site_information request
       function site_information() {

           var messageid = CreateGuid();
           var currenttimestamp = CreateDate();
           var data = {
               "v1": {
                   "returnChannel": responseChannel,
                   "messageID": messageid,
                   "siteID": siteId,
                   "currentTimeStamp": currenttimestamp
               },
               "message": "site_information"
           };
           return data;
       };

         //list_inventory request
         function list_categories() {

             var messageid = CreateGuid();
             var currenttimestamp = CreateDate();
             var data = {
                 "v1": {                   
                     "returnChannel": responseChannel,
                     "messageID": messageid,
                     "siteID": siteId,
                     "minimumAvailable": 1,
                     "includeUsed": false,
                     "includeNew": false,
                     "currentTimeStamp": currenttimestamp
                 },
                 "message": "list_categories"
             };
             return data;
         };

         this.list_modelsReq = function (productCodeID) {
            
             var data = list_models(productCodeID);
             var publication = client.publish(responseChannelPrefix, data).then(function () {
                 console.log('Successfully published!');
             }, function (error) {
                 console.log('Error publishing: ' + error.message);

             });
         };

       


      
         this.list_inventoryReq= function(modelID) {
             var data = list_inventory(modelID);
             var publication = client.publish(responseChannelPrefix, data).then(function () {
                 console.log('Successfully published!');
             }, function (error) {
                 console.log('Error publishing: ' + error.message);

             });
         };


         //list_inventory
         function list_models(productCodeID) {

             var messageid = CreateGuid();
             var currenttimestamp = CreateDate();
             var data = {
                 "v1": {
                     "returnChannel": responseChannel,
                     "messageID": messageid,
                     "siteID": siteId,
                     "pcid": productCodeID,
                     "minimumAvailable": 1,
                     "includeUsed": true,
                     "includeNew": true,
                     "currentTimeStamp": currenttimestamp,

                 },
                 "message": "list_models"
             };
             return data;
         };

         //list_inventory
         function list_inventory(modelID) {

             var messageid = CreateGuid();
             var currenttimestamp = CreateDate();
             var data = {
                 "v1": {
                     "returnChannel": responseChannel,
                     "messageID": messageid,
                     "siteID": siteId,
                     "id":"",     
                     "modid": modelID,
                     "currentTimeStamp": currenttimestamp,        
                 },
                 "message": "list_inventory"
             };
             return data;
         };
      
       

         // random128

         function random128() {

             var result = "";
             for (var i = 0; i < 8; i++)
                 result += String.fromCharCode(Math.random() * 0x10000);
             //var salt = Base64.encode(result);
             return result;
         };

     }
     catch (e) {
         // errorMessage redirect to error page.
             $state.path('/error').search('ref', e.message);
             $rootScope.Loaded = true;
    
     }

 }]);









