angular.module("app")
    .controller("loginCtrl", ["$scope", "$rootScope", "$location", "idealsApi", "$cookieStore", "SharedService", "$http", "$window", "configDetailsProvider","$timeout", "$q", function($scope, $rootScope, $location, idealsApi, $cookieStore, SharedService, $http, $window, configDetailsProvider,$timeout, $q) {

//
//    .controller("loginCtrl", ["$scope", "$rootScope", "$location", "idealsApi", "$cookieStore", "deviceReadyService", "SharedService", "$http", "$window", "configDetailsProvider","$timeout", "$q", function($scope, $rootScope, $location, idealsApi, $cookieStore, deviceReadyService, SharedService, $http, $window, configDetailsProvider,$timeout, $q) {

        $scope.userName = "";
        $scope.password = "";
        $scope.data = "";

        try {
            $rootScope.message = "";
            $rootScope.customerInfo = "";
            $rootScope.storeINFO = "";
            $rootScope.firstName = "";
            $rootScope.lastName = "";
            $scope.userName = "";
            $scope.password = "";


            $rootScope.isEnableStoreSel = sessionStorage.isEnableStoreSel;
            $rootScope.setValue = sessionStorage.setValue;
            console.log($scope.setValue);
                              
                              
                              

                              
                              
                              
                              

            //  $rootScope.customerInfo= sessionStorage.customerInformation;
            if (sessionStorage.customerInformation != null && sessionStorage.customerInformation != "") {
                $rootScope.firstName = JSON.parse(sessionStorage.customerInformation).firstName;
                $rootScope.lastName = JSON.parse(sessionStorage.customerInformation).lastName;
            }

            console.log($rootScope.customerInfo);

//            $rootScope.storeINFO = sessionStorage.storeINFORMATION;
            $scope.testLocationList = function(store_name) {

                sessionStorage.setItem("siteID", "");
                $rootScope.storeINFO = "";
                $rootScope.customerInfo = "";
                $rootScope.firstName = "";
                $rootScope.lastName = "";
                $scope.userName = "";
                $scope.password = "";
                $rootScope.apiErrorMessage = "";
                $rootScope.message = "";
                sessionStorage.storeINFORMATION = "";
                sessionStorage.customerInformation = "";
                $scope.loginForm.userName.$dirty = false;
                $scope.loginForm.userName.$pristine = true;
                $scope.loginForm.password.$dirty = false;
                $scope.loginForm.password.$pristine = true;
                $scope.loginForm.$submitted = false;


                console.log("site id=" + store_name.number);
                SharedService.setData(store_name.number);

                $scope.selectStoreMsg = "Your Store Is Selected";
                idealsApi.site_informationReq(parseInt(SharedService.getData()));


// Change for Session Remove F070818 // may require to remove username & password
//                angular.element('.add-details').ready(function () {
//                sessionStorage.removeItem("userName");
//                sessionStorage.removeItem("passWord");
//                });

            $scope.$watch(function() {
                return $rootScope.messageReceived;
            }, function(newVal, oldVal) {
            if (newVal.data.v1.siteID == sessionStorage.siteID) {
                angular.element('.add-details').ready(function () {
                sessionStorage.removeItem("userName");
                sessionStorage.removeItem("passWord");

                });

            }
            });


            };

            setKeysfunc(configDetailsProvider.apiConnect)

            function setKeysfunc(data1) {
                var data = {
                    "token": data1.token,
                    "group": data1.group
                };

                var url = data1.url;
                var apiKey = data1.key;
                var authentication = SHA256(apiKey + JSON.stringify(data));
                var config = {
                    headers: {
                        "Content-Type": "application/json",
                        "x-authentication": authentication,
                    }
                };

                $http.post(url, data, config)
                    .success(function(data, status, headers, config) {
                        $scope.data = JSON.parse(data.data);
                        $scope.isDisableSignup=false;
                        sessionStorage["store_list"] = JSON.stringify(data);
                        localStorage["store_listFirst"] = JSON.stringify(data);
                        console.log("json data =  " + $scope.data);
                        console.log("json data =  " + data.data);


                    })
                    .error(function(data, status, headers, config) {

                        $rootScope.message = "Data: " + data +
                            "\n status: " + status.toString() +
                            "\n headers: " + headers.toString() +
                            "\n config: " + config.toString();
                        console.log("Data: " + data + "\n status: " + status.toString() +
                            "\n headers: " + headers.toString() +
                            "\n config: " + config.toString());
                    });
            }

            // function to submit the form after all validation has occurred
            $scope.submitLogin = function(check) {
                // check to make sure the form is completely valid
                if ($scope.loginForm.$valid) {

                    $rootScope.mesgGreen=false;
                    $rootScope.mesgRed=false;

                    $rootScope.rememberMe = check;
                    $rootScope.ShowLoader = true;

                    $scope.Name = "";

                    $rootScope.login = {
                        username: $scope.userName,
                        password: $scope.password
                    };

                    if ($rootScope.login.username != "" && $rootScope.login.password != "") {
                        idealsApi.authenticateReq($rootScope.login.username, $rootScope.login.password);
                        var data = "";
                    };
                };
            };


            //redirect to RetrieveLoginInfo page.
            $scope.RetrieveLoginInfo = function() {
                $rootScope.message = "";

                if(SharedService.getData())
                {
                   $location.path("/RetrieveLoginInfo");
                }

            };

            //redirect to RetrieveLoginInfo page.
            $scope.signUp = function() {
                $rootScope.message = "";
                 if(SharedService.getData())
                                              {
                      $location.path("/signUp");
                  }
            };

            // check error message
            if ($rootScope.message != "") {
                $scope.ShowLoader = false;
                $scope.error = $rootScope.message;
            }

          $scope.nestedItemsLevel1 = $scope.data;
          $scope.item.store_name = $scope.nestedItemsLevel1[0];

          $scope.level1Options = {
              onSelect: function (item) {
                  var items = [];
                  for (var i = 1; i <= 5; i++) {
                      items.push(item + ': ' + 'Nested ' + i);
                  }
                  $scope.nestedItemsLevel2 = items;
              }
          };

          $scope.nestedItemsLevel2 = [];
          $scope.level1Options.onSelect($scope.nestedItemsLevel1[0]);

        } catch (e) {
            console.log(e);
        }

    }]);
