﻿angular.module("app").controller("paySuccessCtrl", ["$scope", "$rootScope", "$state", "idealsApi", "$cookieStore", function($scope, $rootScope, $state, idealsApi, $cookieStore) {
    try {

        // check api response
        $rootScope.IsVisible = false;
        $rootScope.message = "";

        // get cust_information api method response and bind data  on html table
        $scope.Name = sessionStorage.firstName + " " + sessionStorage.lastName;
        $scope.address = sessionStorage.address + "," + " " + sessionStorage.city + "," + " " + sessionStorage.state + " " + sessionStorage.zipCode;
        $scope.email=sessionStorage.email;
        $scope.phoneNumber=sessionStorage.phoneNumber;

        if (sessionStorage["cartInfo"] != "") {
            var count = 0;
            var countMinDeposit = 0;
            $scope.cart = JSON.parse(sessionStorage["cartInfo"] || "{}");
            for (var item = 0; item < $scope.cart.length; item++) {
               // count = count + parseFloat($scope.cart[item].price * $scope.cart[item].quantity);
                countMinDeposit = countMinDeposit + parseFloat($scope.cart[item].minimumDeposit);
            };

          //  $scope.depositAmount = "$" + (count).toFixed(2);
            $scope.MinimumDepositAmount = "$" + (countMinDeposit).toFixed(2);
        };

        var collect_depositResponse = JSON.parse(sessionStorage["collect_deposit"] || "{}");

        $scope.approvalCode = collect_depositResponse.data.v1.approvalCode;
        $scope.paidAmount = $scope.MinimumDepositAmount;
        $scope.receiptNumber = collect_depositResponse.data.v1.receiptNumber;

        // print page view
        $scope.printDirective = function(printSectionId) {
            var innerContents = document.getElementById(printSectionId).innerHTML;
            var popupWinindow = window.open("", "_blank", "width=1000,height=1000,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no");
            popupWinindow.document.open();
            popupWinindow.document.write('<html><head><style>table.right_align_3 th {text-align: left;}</style><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
            popupWinindow.document.close();
        };

        $scope.start = function() {
            sessionStorage["selectedDept"] = "";
            sessionStorage["selectedProdN"] = "";
            sessionStorage["selectedModl"] = "";
            sessionStorage["cartInfo"] = "";
            $rootScope.cartNumber = 0;
            $state.go("start");
        }
    } catch (e) {

    }
}]);