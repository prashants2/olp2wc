﻿
angular.module('app')
 .controller('accountCtrl', ['$scope', '$rootScope', '$location', 'idealsApi', function ($scope, $rootScope, $location, idealsApi) {

     var SelValue = "";
     var countpay = 0;
     var person;

     $scope.isVisibleRemoveAll = true;
     try {

         $rootScope.message = "";
         $scope.userFLName = sessionStorage.userFLName;

         $scope.$watch(function () {
             return ($scope.payment, $scope.footerCount);
         }, function (newVal, oldVal) {

             if (newVal) {
                 $scope.IsVisible1 = true;
                 $scope.IsVisible2 = false;
             }
             else {
                 $scope.IsVisible1 = false;
                 $scope.IsVisible2 = true;
             }
         });

         // check message of list_account
         $scope.messageReceived = JSON.parse(sessionStorage['list_accounts'] || '{}');
         if ($scope.messageReceived) {

             var counts = 0;
             var RadSelValue = "";
             $scope.count = 0;
             var json = $scope.messageReceived;
             $scope.businessDate = json.data.v1.businessDate;
             var accountsData = json.data.v1.accounts;
             $scope.account = [];
             var dueDate = "";
             var accountType = "";
             var accountID = "";
             var itemDescription = "";
             var modeOfPayment = "";
             var balanceDue = "";
             var numberOfPayments = "";
             var paymentOptions = "";
             var paymentAllowed;

             if (json.data.v1.forceMinDue == true && sessionStorage.getItem('selectedPayment') == null) {

                 $scope.isVisibleRemoveAll = false;
                 callPayMinDue(accountsData);
             }
             else if(sessionStorage['selectedPayment']) {
               
                 $scope.payment = JSON.parse(sessionStorage['selectedPayment'] || '{}');
                 var count = 0;
                 for (var i = 0; i < $scope.payment.length; i++) {
                     count = count + parseFloat($scope.payment[i].amountOfPayment);
                 };
                 var countpay = $scope.payment.length;
                 $scope.footerCount = "$" + (count).toFixed(2);
             }
             else {
                 countpay = 0;
                 $scope.payment = "";
                 $scope.footerCount = "";
             }



             //count account array length
             for (var i = 0; i < accountsData.length; i++) {
                 //create account information json to bind table
                 accountInfo = {
                     paymentAllowed: accountsData[i].paymentAllowed,
                     amountOfPayment: accountsData[i].subTotal,
                     numberOfPayments: accountsData[i].numberOfPayments,
                     dueDate: accountsData[i].dueDate,
                     accountType: accountsData[i].accountType,
                     accountID: accountsData[i].accountID,
                     itemDescription: accountsData[i].itemDescription,
                     modeOfPayment: accountsData[i].modeOfPayment,
                     paymentOptions: accountsData[i].paymentOptions
                 };

                 $scope.account.push(accountInfo);
             };

             //count total amount to show in html footer.
             for (var i = 0; i < $scope.account.length; i++) {
                 counts = (counts) + parseFloat($scope.account[i].amountOfPayment);
             };

             $scope.count = '$' + (counts).toFixed(2);

             // click pay button to open payment popup window
             $scope.payButton = function (accounts) {

                 $("input:radio").attr("checked", false);
                 SelValue = "";
                 $scope.paymentOption = accounts.paymentOptions;
                 $scope.messageInfo = (accounts.accountType) + " " + accounts.accountID;
                 $('#myModal').modal('show');

             };

             //click detail button to open detail popup window
             $scope.detailButton = function (accounts) {

                 var accountsArr = json.data.v1.accounts;
                 for (var i = 0; i < accountsArr.length; i++) {

                     if (accounts.accountID == accountsArr[i].accountID) {
                         $scope.messageInfo = (accountsArr[i].accountType) + " " + accountsArr[i].accountID;
                         $scope.detailInventory = accountsArr[i].inventory;
                         $scope.paymentFreq = accountsArr[i].modeOfPayment;
                         $scope.RemBal = '$' + accountsArr[i].balanceDue;

                         $scope.paymentallowed = accountsArr[i].paymentAllowed;


                         if (parseFloat(typeof (accountsArr[i].term) == "undefined" ? 0 : (accountsArr[i].term)) == 0.0) {
                             $scope.RemBal = 0;
                             $scope.terms = (parseFloat(typeof (accountsArr[i].rentalRate) == "undefined" ? 0 : (accountsArr[i].rentalRate)) + parseFloat(typeof (accountsArr[i].fees) == "undefined" ? 0 : (accountsArr[i].fees)) + parseFloat(typeof (accountsArr[i].taxes) == "undefined" ? 0 : (accountsArr[i].taxes))).toFixed(2) + " = Rate: $" + parseFloat(typeof (accountsArr[i].rentalRate) == "undefined" ? 0 : (accountsArr[i].rentalRate)) + " + fees: $" + parseFloat(typeof (accountsArr[i].fees) == "undefined" ? 0 : (accountsArr[i].fees)) + " + Tax: $" + parseFloat(typeof (accountsArr[i].taxes) == "undefined" ? 0 : (accountsArr[i].taxes));
                         }
                         else {
                             $scope.terms = parseFloat(typeof (accountsArr[i].term) == "undefined" ? 0 : (accountsArr[i].term)) + " Payments at  $" + (parseFloat(typeof (accountsArr[i].rentalRate) == "undefined" ? 0 : (accountsArr[i].rentalRate)) + parseFloat(typeof (accountsArr[i].fees) == "undefined" ? 0 : (accountsArr[i].fees)) + parseFloat(typeof (accountsArr[i].taxes) == "undefined" ? 0 : (accountsArr[i].taxes))).toFixed(2) + " = Rate: $" + parseFloat(typeof (accountsArr[i].rentalRate) == "undefined" ? 0 : (accountsArr[i].rentalRate)) + " + fees: $" + parseFloat(typeof (accountsArr[i].fees) == "undefined" ? 0 : (accountsArr[i].fees)) + " + Tax: $" + parseFloat(typeof (accountsArr[i].taxes) == "undefined" ? 0 : (accountsArr[i].taxes));
                         }

                         break;
                     };
                 };

                 $('#paymentDetail').modal('show');
             };

             //check radio button
             $scope.selectRadio = function (value) {
                 SelValue = value;
             };

             var amountOfPayment1 = "";
             var nextDueDate1 = "";
             var accountID1 = "";
             var accountType1 = "";
             var itemDescription1 = "";


             //select payment detail from pay popup window and selected information bind in "selected_payment" table
             $scope.pay = function () {
                 $scope.disableButton = false;

                 if (SelValue) {
                     for (var i = 0; i < $scope.paymentOption.length; i++) {
                         var obj = $scope.paymentOption[i];
                         if (obj.credits === SelValue) {
                             amountOfPayment1 = obj.amountOfPayment;
                             nextDueDate1 = obj.nextDueDate;
                             credits1 = obj.credits,
                             optionID1 = obj.optionID,
                             paymentDescription1 = obj.paymentDescription
                             break;
                         }
                     }
                     for (var k = 0; k < accountsData.length; k++) {
                         var payOption = accountsData[k].paymentOptions;
                         if (payOption === $scope.paymentOption) {
                             accountID1 = accountsData[k].accountID;
                             accountType1 = accountsData[k].accountType;
                             itemDescription1 = accountsData[k].itemDescription;
                             break;
                         }
                     }
                     if (countpay == 0) {
                         person = [{
                             itemDescription: itemDescription1,
                             amountOfPayment: amountOfPayment1,
                             nextDueDate: nextDueDate1,
                             accountID: accountID1,
                             accountType: accountType1,
                             credits: credits1,
                             optionID: optionID1,
                             paymentDescription: paymentDescription1
                         }];
                         $scope.payment = person;
                         countpay++;
                         footerCount();
                     }
                     else {
                         var i = 0;
                         for (i = 0; i < $scope.payment.length; i++) {
                             if ($scope.payment[i].accountID === accountID1) {
                                 var amount = ($scope.payment[i].amountOfPayment);
                                 if ($scope.payment[i].amountOfPayment == amountOfPayment1) {
                                     break;
                                 }
                                 else {
                                     removeUpdRow($scope.payment[i].accountID);

                                     if ($scope.payment) {
                                         person = {
                                             itemDescription: itemDescription1,
                                             amountOfPayment: amountOfPayment1,
                                             nextDueDate: nextDueDate1,
                                             accountID: accountID1,
                                             accountType: accountType1,
                                             credits: credits1,
                                             optionID: optionID1,
                                             paymentDescription: paymentDescription1
                                         };
                                         $scope.payment.push(person);
                                     }
                                     else {
                                         person = [{
                                             itemDescription: itemDescription1,
                                             amountOfPayment: amountOfPayment1,
                                             nextDueDate: nextDueDate1,
                                             accountID: accountID1,
                                             accountType: accountType1,
                                             credits: credits1,
                                             optionID: optionID1,
                                             paymentDescription: paymentDescription1
                                         }];
                                         $scope.payment = person;
                                         countpay++;
                                     }
                                     footerCount();
                                     break;
                                 }
                             }
                         }

                         if (i == $scope.payment.length) {
                             person = {
                                 itemDescription: itemDescription1,
                                 amountOfPayment: amountOfPayment1,
                                 nextDueDate: nextDueDate1,
                                 accountID: accountID1,
                                 accountType: accountType1,
                                 credits: credits1,
                                 optionID: optionID1,
                                 paymentDescription: paymentDescription1
                             };
                             $scope.payment.push(person);
                             footerCount();
                         }
                     };
                 }
             };


             //remove selected row
             removeUpdRow = function (accountID) {
                 var index = -1;
                 var paymentArr = eval($scope.payment);
                 for (var i = 0; i < paymentArr.length; i++) {
                     if (paymentArr[i].accountID === accountID) {
                         index = i;
                         break;
                     }
                 }
                 if (index === -1) {
                     alert("Something gone wrong");
                 }
                 $scope.payment.splice(index, 1);

             };

             //remove row 
             $scope.removeRow = function (accountID) {
                 var index = -1;
                 var paymentArr = eval($scope.payment);
                 for (var i = 0; i < paymentArr.length; i++) {
                     if (paymentArr[i].accountID === accountID) {
                         index = i;
                         break;
                     }
                 }
                 if (index === -1) {
                     alert("Something gone wrong");
                 }
                 $scope.payment.splice(index, 1);

                 footerCount();
             };

             //count footer of selected payment amount
             function footerCount() {
                 var fcount = 0;
                 var paymentArr = eval($scope.payment);

                 if (paymentArr) {
                     for (var i = 0; i < paymentArr.length; i++) {
                         fcount = fcount + (parseFloat(paymentArr[i].amountOfPayment));
                     }
                 }
                 else {
                     $scope.payment = "";
                 }

                 if (fcount == 0) {
                     $scope.footerCount = "";
                 }
                 else {
                     $scope.footerCount = '$' + fcount.toFixed(2);
                 }
             };


             //remove all select payment 
             $scope.removeALLRow = function () {
                 countpay = 0;
                 $scope.payment = "";
                 $scope.footerCount = "";
                 //footerCount();
             };


             // payMinDue functionlity based on condition
             $scope.payMinDue = function () {             
                 callPayMinDue(accountsData);
             };



             //checkout click to send payment detail and call list_store_card api method.
             $scope.checkOut = function () {
                 sessionStorage['selectedPayment'] = JSON.stringify($scope.payment);
                 $scope.Selamount = [];
                 $scope.Selamount = $scope.payment.slice(0);
                 var convenfree =
                       {
                           itemDescription: "Convenience Fee",
                           accountID: "",
                           accountType: "",
                           amountOfPayment: $scope.messageReceived.data.v1.convenienceFee,
                           nextDueDate: "",
                           credits: "",
                           optionID: "",
                           paymentDescription: ""
                       };

                 $scope.Selamount.push(convenfree);
                      
                 sessionStorage['paymentAmt'] = JSON.stringify($scope.Selamount);
                 //sessionStorage.paymentAmt = $scope.payment;
                 sessionStorage.businessDate = $scope.businessDate;
                 sessionStorage.convenienceFree = $scope.messageReceived.data.v1.convenienceFee;
                 //$rootScope.paymentAmt = $scope.payment;
                 //$rootScope.businessDate = $scope.businessDate;
                 idealsApi.list_stored_cardsReq();
             };

             //logout click
             $scope.logoutClick = function () {
                 idealsApi.logoutReq();
             };

             // call payment_history api method 
             $scope.PaymentHistory = function () {
                 idealsApi.payment_historyReq();
             }

             function callPayMinDue(accountsData) {
          
                 var isFirst = true;
                 for (var i = 0; i < accountsData.length; i++) {
                     if (accountsData[i].subTotal > 0 && accountsData[i].paymentAllowed == true) {
                         if (isFirst) {
                             $scope.payment = [];
                         }
                         $scope.disableButton = true;
                         person = {
                             itemDescription: accountsData[i].itemDescription,
                             accountID: accountsData[i].accountID,
                             accountType: accountsData[i].accountType,
                             amountOfPayment: accountsData[i].paymentOptions[0].amountOfPayment,
                             nextDueDate: accountsData[i].paymentOptions[0].nextDueDate,
                             credits: accountsData[i].paymentOptions[0].credits,
                             optionID: accountsData[i].paymentOptions[0].optionID,
                             paymentDescription: accountsData[i].paymentOptions[0].paymentDescription
                         };
                         $scope.payment.push(person);
                         isFirst = false;
                     }
                 }
                 footerCount();
             }
         }
     }
     catch (e) {
         console.log(e);
     }

 }]);