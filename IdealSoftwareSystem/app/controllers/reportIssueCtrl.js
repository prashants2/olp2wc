﻿angular.module('app')
 .controller('reportIssueCtrl', ['$scope', '$rootScope', 'idealsApi','$http', '$location', '$cookieStore', function ($scope, $rootScope, idealsApi,$http, $location, $cookieStore) {

     try {
         $rootScope.message = "";
         $scope.submit = function () {
             if ($scope.reportIssue.$valid) {
                 $rootScope.message = "";
               
                 var reportIssue_data = {
                     "email_address": $scope.email,
                     "first_name": $scope.first_name,
                     "last_name": $scope.last_name,
                     "subject": $scope.subject,
                     "description": $scope.description,
                     "location_url": ($cookieStore.get('api_connect')).location_url
                 };

                 var report_url = ($cookieStore.get('api_connect')).report_url;
                 var config = {
                     headers: {
                         'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                     }
                 };

                 $http.post(report_url, reportIssue_data, config)
                     .success(function (data, status, headers, config) {
                         //$location.path('/reportIssueSubmit');
                         //$rootScope.message = "Your Issue Has Been Submitted";

                         debugger;
                         $("#myModal").modal({
                             backdrop: 'static',
                             keyboard: false
                         });

                         $('#myModal').modal('show');
                     })
                     .error(function (data, status, header, config) {
                      
                         $rootScope.message = "Data: " + data +
                             "<hr />status: " + status +
                             "<hr />headers: " + header +
                             "<hr />config: " + config;
                     });
             };
         };

         $scope.logoutClick = function () {
             $rootScope.message = "";
             idealsApi.logoutReq();
         };

         $scope.back = function () {
             $rootScope.message = "";
             window.history.back();
         };

         $scope.ok = function () {
             $rootScope.message = "";
             window.history.back();
         };


     }
     catch (e) {
         console.log(e);
     }

 }]);