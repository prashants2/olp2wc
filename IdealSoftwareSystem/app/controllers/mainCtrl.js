﻿angular.module('app')
 .controller('mainCtrl', ['$scope', '$rootScope', '$location', '$route', function ($scope, $rootScope, $location, $route) {

     $rootScope.messageReceived = { data: { message: '' } };
     $rootScope.errorCount = 0;
     $rootScope.isLogin = false;
     $rootScope.isSubscribe = false;
     $rootScope.ErrorMessage = "";
     sessionStorage.versionNo = "2.0";
     $rootScope.versionNo = sessionStorage.versionNo;
     try
     { 
         // check api response 
         $rootScope.message = "";
         $scope.$watch(function () {
             return $rootScope.messageReceived;
         }, function (newVal, oldVal) { 
          
             if (newVal.data.message === 'connect') {               
                 $rootScope.isSubscribe = false;
                 $rootScope.message = "";
                 $rootScope.Loaded = true;
             }

             //check site_information api method response
             if (newVal.data.message === 'site_information') {
              
                 $rootScope.message = "";
                 if (newVal.data.v1.errorDescription === 'Successful') {
                     sessionStorage.checkStoreInformation = "have value";
                     sessionStorage.currentTimeStamp = newVal.data.v1.currentTimeStamp;
                     $rootScope.storeINFO = "Store: " + newVal.data.v1.address + " " + newVal.data.v1.city + ", " + " " + newVal.data.v1.state + " " + newVal.data.v1.zip + " " + "Phone: " + newVal.data.v1.phone;
                 }
                 else {
                    
                     $rootScope.message = newVal.data.v1.errorDescription;
                     $rootScope.ShowLoader = false;
                   
                 }
             }

             //check authenticate api method response
             if (newVal.data.message === 'authenticate') {
                 $rootScope.isLogin = true;
                 $rootScope.message = "";
                 if (newVal.data.v1.errorDescription === 'Invalid Username and/or Password') {
                    
                     $rootScope.errorCount++;
                     $rootScope.ShowLoader = false;
                     //$route.reload();
                     $rootScope.message = newVal.data.v1.errorDescription;
                     if ($rootScope.errorCount >=3 ) {
                         $rootScope.message = "Invalid username and/or password. If you have forgotten your username and/or password try retrieving it by following the 'Forgot Password' link. If you have not signed up to use this site, you may do so by clicking the Sign Up button.";
                     }

                 }
                 else if (newVal.data.v1.errorDescription === 'Successful')
                 {                    
                     $rootScope.errorCount = 0;
                     sessionStorage.loggedUser = "valid";
                 }
                 else {
                     $rootScope.message = newVal.data.v1.errorDescription;
                     $rootScope.errorCount = 0;
                     $rootScope.ShowLoader = false;
                   
                 }
             }

             //check list_accounts api method response
             if (newVal.data.message === 'list_accounts') {
                 $rootScope.message = "";
                 $rootScope.ShowLoader = false;
                 if (newVal.data.v1.errorDescription === 'Successful') {
                     sessionStorage.removeItem('selectedPayment');
                     sessionStorage.userFLName = newVal.data.v1.customerInfo.firstName + " " + newVal.data.v1.customerInfo.lastName;
                     sessionStorage.userName = newVal.data.v1.customerInfo.userName;
                     sessionStorage['list_accounts'] = JSON.stringify($rootScope.messageReceived);
                     $location.path('/account');
                 }
                 else {
                     $rootScope.ShowLoader = false;
                     $rootScope.message = newVal.data.v1.errorDescription;
                   
                 }
             }
             //check logout api method response
             if (newVal.data.message === 'logout') {
                 if (newVal.data.v1.errorDescription === 'Successful')
                 {
                     $rootScope.message = "";
                     sessionStorage.loggedUser = 'null';
                     $location.path('/login');
                 }
                 else
                 {
                     $rootScope.message = "";
                     $rootScope.ShowLoader = false;
                     $route.reload();
                     $rootScope.message = newVal.data.v1.errorDescription;
                 }         
             }

             //check update_account api method response
             if (newVal.data.message === 'update_account') {
                 $rootScope.message = "";
                 newVal.data.message = "";
                 $rootScope.ShowLoader = false;
                 if (newVal.data.v1.errorDescription === 'Successful') {
                     $rootScope.accountSubmitMessage = "Your Account has been updated.";
                     $location.path('/changeAcSubmit');
                 
                 }
                 else {
                     $rootScope.message = "";
                     $rootScope.ShowLoader = false;
                     $route.reload();
                     $rootScope.message = newVal.data.v1.errorDescription;
                 }
             }

             //check reset_password api method response
             if (newVal.data.message === 'reset_password') {
                 $rootScope.message = "";
                 $rootScope.ShowLoader = false;
                 if (newVal.data.v1.errorDescription === 'Successful') {
                     $rootScope.accountSubmitMessage = "Your Account has been updated.";
                     $location.path('/changeAcSubmit');
                     //$rootScope.message = newVal.data.v1.errorDescription + "   tempPassword=" + newVal.data.v1.tempPassword;
                 }
                 else {
                     $rootScope.message = newVal.data.v1.errorDescription;
                     $rootScope.ShowLoader = false;
                     $route.reload();
                 }
             }
      

             //check signup api method response
             if (newVal.data.message === 'signup') {
                 $rootScope.message = "";
                 $rootScope.ShowLoader = false;
                 if (newVal.data.v1.errorDescription === 'Successful') {
                     $rootScope.errorCount = 0;
                     $rootScope.accountSubmitMessage = "Your Account has been created. Please make a note of your username and password.";
                     $location.path('/changeAcSubmit');
                 }
                 else {
                     $rootScope.ShowLoader = false;
                     $rootScope.message = newVal.data.v1.errorDescription;
                     $route.reload();
                     //$location.path('/signUp');
                 }
             }

             //check list_stored_cards api method response
             if (newVal.data.message === 'list_stored_cards') {
                 $rootScope.message = "";
                 $rootScope.ShowLoader = false;
                 if (newVal.data.v1.errorDescription === 'Successful') {
                     if (newVal.data.v1.cards!=null)
                     {
                         sessionStorage['list_cards'] = JSON.stringify($rootScope.messageReceived);
                       
                     }
                     else
                     {
                         sessionStorage['list_cards'] = null;
                     }
              
                     $rootScope.errorCount = 0;
                     $location.path('/checkInfo');
                 }
                 else {
                     $rootScope.ShowLoader = false;
                     $rootScope.message = newVal.data.v1.errorDescription;
                     $route.reload();
                     //$location.path('/signUp');
                 }
             }

             //check pay_stored_card api method response
             if (newVal.data.message === 'pay_stored_card') {
                 $rootScope.message = "";
                 sessionStorage.removeItem('selectedPayment');
                 $rootScope.ShowLoader = false;
                 if (newVal.data.v1.errorDescription === 'Successful') {
                     sessionStorage['pay_stored_card'] = JSON.stringify($rootScope.messageReceived);
                     $rootScope.errorCount = 0;
                     //$location.path('/makePayment');
                 }
                 else {
                     alert("paymentDecline");
                     $rootScope.ShowLoader = false;
                     $rootScope.message = newVal.data.v1.errorDescription;
                     //$route.reload();
                     $location.path('/paymentDecline');
                 }
    
             };

                 // for CardConnect start    


             //check pay_stored_card api method response
             if (newVal.data.message === 'pay_stored_cardcc') {
                 alert("paymentDecline");
                 $rootScope.message = "";
                 sessionStorage.removeItem('selectedPayment');
                 $rootScope.ShowLoader = false;
                 if (newVal.data.v1.errorDescription === 'Successful') {
                     sessionStorage['pay_stored_cardcc'] = JSON.stringify($rootScope.messageReceived);
                     $rootScope.errorCount = 0;
                     $location.path('/makePayment');
                 }
                 else {
                     alert("paymentDecline");
                     $rootScope.ShowLoader = false;
                     $rootScope.message = newVal.data.v1.errorDescription;
                     //$route.reload();
                     $location.path('/paymentDecline');
                 }

             };



                 // End for cardConnect

             //check customer_information api method response
             if (newVal.data.message === 'customer_information') {
                 $rootScope.message = "";
                 $rootScope.ShowLoader = false;
                 if (newVal.data.v1.errorDescription === 'Successful') {
                     sessionStorage['cust_info'] = JSON.stringify($rootScope.messageReceived);
                     $rootScope.errorCount = 0;
                     $location.path('/makePayment');
                 }
                 else {
                     $rootScope.ShowLoader = false;
                     $rootScope.message = newVal.data.v1.errorDescription;
                     $route.reload();
                 }
             };

             //check payment_history api method response
             if (newVal.data.message === 'payment_history') {

                 $rootScope.message = "";
                 if (newVal.data.v1.errorDescription === 'Successful') {
                     sessionStorage['pay_hist'] = JSON.stringify($rootScope.messageReceived);
                     $rootScope.errorCount = 0;
                     $location.path('/PaymentHistory');
                 }
                 else {
                     $rootScope.ShowLoader = false;
                     $rootScope.message = newVal.data.v1.errorDescription;
                     $route.reload();
                     //$location.path('/signUp');
                 }
             };

         });
     }
     catch(e)
     {
         console.log(ew);
     }

}]);