angular.module('app')
    .controller('checkCtrl', ['$scope', '$rootScope', 'idealsApi', '$location', '$cookieStore', function ($scope, $rootScope, idealsApi, $location, $cookieStore) {
   
     try {

         $('#isMsgEnable').show();
         $('#showMsg').hide();
         $rootScope.message = "";
         $scope.userFLName = sessionStorage.userFLName;
         var result = "";
         $scope.isSaving = false;
         $scope.cvvLength = 3;
         $scope.cardLength = 16;
         loadIframe();
         $scope.checkToken = "";

         $scope.currentYear = new Date().getFullYear();
         $scope.currentMonth = new Date().getMonth() + 1;
         var cookieWObject = ($cookieStore.get('payeezy_auth'));

         if (sessionStorage['paymentAmt'] !="")
         {
             var count = 0;

             $scope.payment = JSON.parse(sessionStorage['paymentAmt'] || '{}');
             for (var i = 0; i < $scope.payment.length; i++) {
                 count = count + parseFloat($scope.payment[i].amountOfPayment);
             };

             $scope.totalAmount = (count).toFixed(2);
             $scope.total = "$" + (count).toFixed(2);
         };
     
         if ($rootScope.disable) {
             $scope.isEnable1 = false;
             $scope.isEnable2 = true;
             $scope.isEnable3 = false;
         }
         else {

             if ($rootScope.checkProcessor) {
                 if ($rootScope.checkProcessor === 'payeezy') {
                     $scope.isEnable1 = true;
                     $scope.isEnable2 = false;
                     $scope.isEnable3 = false;
                 }
                 if ($rootScope.checkProcessor === 'cardconnect') {
                     $scope.isEnable1 = false;
                     $scope.isEnable2 = false;
                     $scope.isEnable3 = true;
                 }
             }

         }

         $scope.verifyCard = function (card_type) {

             $scope.cardholderName = "";
             $scope.cardNumber = "";
             $scope.exp_month = "";
             $scope.exp_year = "";
             $scope.cvvCode = "";
             $scope.zip_postal_code = "";

             $scope.paymentinfoform.cardholder_Name.$dirty = false;
             $scope.paymentinfoform.cardholder_Name.$pristine = true;

             $scope.paymentinfoform.card_Number.$dirty = false;
             $scope.paymentinfoform.card_Number.$pristine = true;

             $scope.paymentinfoform.cvv_code.$dirty = false;
             $scope.paymentinfoform.cvv_code.$pristine = true;

             $scope.paymentinfoform.zip_postal_code.$dirty = false;
             $scope.paymentinfoform.zip_postal_code.$pristine = true;

             $scope.paymentinfoform.$submitted = false;

             if (card_type == "American Express") {
                 $scope.cardLength = 15;
                 $scope.cvvLength = 4;
             }
             else {
                 $scope.cardLength = 16;
                 $scope.cvvLength = 3;
             }
         }

         $scope.cardTypes = [
             "Visa",
             "Mastercard",
             "American Express",
             "Discover"
         ]
   
         if (sessionStorage['cards'] != "")
         {     
             $scope.items = JSON.parse(sessionStorage['cards'] || '{}');
         }
                

         //log out click
         $scope.logoutClick = function () {
             idealsApi.logoutReq();
         };

         // call customer_information method of api.

         $scope.MakePayment_NC = function () {
         
             var myNumber = $scope.exp_month;
             var formattedNumber = ("0" + myNumber).slice(-2);
             $scope.exp_month = formattedNumber;
             
             $('#hideMsg').hide();
             $('#showMsg').show();
             var $form = $('#paymentinfoform'); 
             $('#someHiddenDiv').hide();

             if ($scope.paymentinfoform.$valid) {

                 $rootScope.ShowLoader = true;
                 $scope.isSaving = true;
                 $form.find('#paymentinfo').prop('disabled', true);          
        
                 var apiKey = cookieWObject.api_key;

                 var js_security_key = cookieWObject.payeezy_key;

                 var ta_token = cookieWObject.ta_token;

                 //var auth = true;
                 var payeezy_auth = true;

                 Payeezy.setApiKey(apiKey);

                 Payeezy.setJs_Security_Key(js_security_key);

                 Payeezy.setTa_token(ta_token);

                 //Payeezy.setAuth(auth);
                 Payeezy.setAuth(payeezy_auth);

                 Payeezy.createToken(responseHandler, ($cookieStore.get('api_connect')).env);

                 $('#someHiddenDiv').show();

                 return false;   
             }
         };


         var tokenError = '';

         function loadIframe() {


             var srvs = document.getElementById("tokenframe").src = "https://ideal.cardconnect.com:" + (($cookieStore.get('api_connect')).env === 'development' ? '6443' : '8443') + "/itoke/ajax-tokenizer.html?css=input%7Bwidth%3A232px%3Bheight%3A30px%3Bmargin-top%3A-7px%3Bmargin-left%3A-8px%3Bpadding-left%3A10px%3B%7D%252Eerror%7Bcolor%3Ared%3Bborder-color%3Ared%3B%7D&invalidinputevent=true";
         }
         $scope.cc_NewCardPayment = function () {             
            // debugger;
             if ($scope.cc_paymentinfoform.$valid) {
                 $rootScope.ShowLoader = true;
                 var cvvCode = $scope.cvvCode;
                 var expYear = $scope.exp_year;
                 var expDate = $scope.exp_month;
                 var cardNumber = $scope.cardNumber;
                 var cardholderName = $scope.cardholderName;
                 var cardType = $scope.cardType;
                 var Cardtoken = document.getElementById('token').value;
                 var cardTokenLength = Cardtoken.length;
                 var processType = "entry";
                 var expDate = expDate + expYear;
                 var first2 = Cardtoken.substr(1, 2);
                 var first6 = first2 + "****";
                 var last4 = Cardtoken.substr((cardTokenLength - 4), 4);
                 var zipCode = $scope.zip_postal_code;
                 var data = idealsApi.pay_stored_cardReq(Cardtoken, expDate, first6, last4, cardholderName, cardType, $scope.payment, $scope.totalAmount, processType, zipCode);                 

                 $rootScope.tokenumber = Cardtoken;
             }
         }


         // >>>>>>>>>>> Card Validation
         window.addEventListener('message', function (event) {             
             console.log("received message: " + event.data);  
             //$scope.checkToken = "";
             var token = JSON.parse(event.data);
             document.getElementById("cardNumberValidationError").innerHTML = '';
             if (token.validationError === undefined && ($scope.cc_paymentinfoform.$valid == true)) {
                document.getElementById("Nccpayment").disabled = false;
                document.getElementById("cardNumberValidationError").innerHTML = '';                          
             }             
             else if(token.message == "")
             {      
                 document.getElementById("Nccpayment").disabled = true;           
                 document.getElementById("cardNumberValidationError").innerHTML = 'Card is invalid';
                 //$scope.cardNumberValidationError = 'Card is invalid';
             }
             tokenError = token.validationError;
             $scope.checkToken = token.message;  
             document.getElementById('token').value = token.message;           
             console.log("token.message >>>>>>>>>>>>>token" + token);             
         }, false);         

         $scope.NonSavedCardc = function () {
             $rootScope.disable = false;
		
             if (cookieWObject.processor_type == 0) {
                 $rootScope.apiErrorMessage = "";
                 $('#payment-errors').html("");
                 $('#paymentinfoform').trigger("reset");
                 $scope.paymentinfoform.cardholder_Name.$dirty = false;
                 $scope.paymentinfoform.cardholder_Name.$pristine = true;

                 $scope.paymentinfoform.card_Number.$dirty = false;
                 $scope.paymentinfoform.card_Number.$pristine = true;

                 $scope.paymentinfoform.cvv_code.$dirty = false;
                 $scope.paymentinfoform.cvv_code.$pristine = true;

                 $scope.paymentinfoform.zip_postal_code.$dirty = false;
                 $scope.paymentinfoform.zip_postal_code.$pristine = true;

                 $scope.paymentinfoform.$submitted = false;

                 $rootScope.message = "";
                 $('#hideMsg').show();
                 $('#showMsg').hide();
                 $scope.isEnable1 = true;
                 $scope.isEnable2 = false;
                 $scope.isEnable3 = false;

             }

             else if (cookieWObject.processor_type == 1) {
                 loadIframe();
                 $rootScope.apiErrorMessage = "";
                 $('#payment-errors').html("");
                 $('#cc_paymentinfoform').trigger("reset");
                 $scope.cc_paymentinfoform.cardholder_Name.$dirty = false;
                 $scope.cc_paymentinfoform.cardholder_Name.$pristine = true;

                 $scope.cc_paymentinfoform.cvv_code.$dirty = false;
                 $scope.cc_paymentinfoform.cvv_code.$pristine = true;

                 $scope.cc_paymentinfoform.zip_postal_code.$dirty = false;
                 $scope.cc_paymentinfoform.zip_postal_code.$pristine = true;

                 document.getElementById("cardNumberValidationError").innerHTML = '';
      

                 $scope.cc_paymentinfoform.$submitted = false;

                 $rootScope.message = "";
                 $('#hideMsg').show();
                 $('#showMsg').hide();
                 $scope.isEnable1 = false;
                 $scope.isEnable2 = false;
                 $scope.isEnable3 = true;

             }
             else {
                 return false;
             }
         }


         $scope.PayOptnSubmit = function () {             
             if (cookieWObject.processor_type == 1) {
                 $scope.cc_NewCardPayment();
             }
             else if (cookieWObject.processor_type == 0) {
                 $scope.MakePayment_NC();
             }
             else {
                 return false;
             }

         };

         $scope.savedCard=function()
         {
             $rootScope.disable = true;
             $rootScope.apiErrorMessage = "";
             $('#payment-errors').html("");
             $('#paymentinfo').trigger("reset");

             $scope.paymentinfoform.cardholder_Name.$dirty = false;
             $scope.paymentinfoform.cardholder_Name.$pristine = true;

             $scope.paymentinfoform.card_Number.$dirty = false;
             $scope.paymentinfoform.card_Number.$pristine = true;

             $scope.paymentinfoform.cvv_code.$dirty = false;
             $scope.paymentinfoform.cvv_code.$pristine = true;

             $scope.paymentinfoform.zip_postal_code.$dirty = false;
             $scope.paymentinfoform.zip_postal_code.$pristine = true;

             $scope.paymentinfoform.$submitted = false;

             $rootScope.message = "";
             $('#hideMsg').show();
             $('#showMsg').hide();
             $scope.isEnable1 = false;
             $scope.isEnable3 = false;
             $scope.isEnable2 = true;
         }


    
         $scope.MakePayment_SC = function (Selectedcard) {
             $('#hideMsg').hide();
             $('#showMsg').show();
             if ($scope.paymentinfo.$valid) {
                 $rootScope.ShowLoader = true;
                 var $form = $('#paymentinfo');
                 for (var i = 0; i < $scope.items.length; i++) {
                     var cardinfo = Selectedcard;
                     if ($scope.items[i].last4 == cardinfo.last4) {
                         var Cardtoken = $scope.items[i].cardToken;
                         var expDate = ($scope.items[i].expDate);
                         var first6 = $scope.items[i].first6;
                         var last4 = $scope.items[i].last4;
                         var cardType = $scope.items[i].cardType;
                         var cardHolder = $scope.items[i].cardHolder;
                         var zipCode = $scope.items[i].zip;
                         var processType = "stored";
                         break;
                     }
                 }
                 $form.find('#paymentinfo').prop('disabled', true);
                 idealsApi.pay_stored_cardReq(Cardtoken, expDate, first6, last4, cardHolder, cardType, $scope.payment, $scope.totalAmount, processType, zipCode);
             }
         };


         var responseHandler = function (status, response) {

             var $form = $('#paymentinfoform');

             if (status != 201) {
                 if (response.Error && status != 400) {
                     var error = response["Error"];
                     var errormsg = error["messages"];
                     var errorcode = JSON.stringify(errormsg[0].code, null, 4);
                     var errorMessages = JSON.stringify(errormsg[0].description, null, 4);

                     $('#payment-errors').html("Error Messages: " + errorMessages);
                 }
                 if (status == 400 || status == 500) {

                     if (status == 500) {
                         var errorMessages = "";
                         var errormsg = response.error;
                         errorMessages = errorMessages + 'Error Messages:'

                             + errormsg;

                     }
                     else {

                         var errormsg = response.Error.messages;
                         var errorMessages = "";
                         for (var i in errormsg) {
                             var ecode = errormsg[i].code;
                             var eMessage = errormsg[i].description;
                             errorMessages = errorMessages + 'Error Code:' + ecode + ', Error Messages:'
                                 + eMessage;
                         }
                     }

                     $('#payment-errors').html(errorMessages);
                 }

                 $('#hideMsg').show();
                 $('#showMsg').hide();
                 $form.find('#NCpayment').prop('disabled', false);
                 $('#imageDiv').hide();
                 $form.find('imageDiv').prop('disabled', true)

             }

             else {
                 $('#someHiddenDiv').hide();
                 result = response.token.value;
                 $form.find('#NCpayment').prop('disabled', true);

                 var cvvCode = $scope.cvvCode;
                 var expYear = $scope.exp_year;
                 var expDate = $scope.exp_month;
                 var cardNumber = $scope.cardNumber;
                 var cardholderName = $scope.cardholderName;
                 var taTokan = $scope.taTokan;
                 var authCheck = $scope.authCheck;
                 var cardType = $scope.cardType;
                 var Cardtoken = result;
                 var processType = "entry";
                 $form.find('button').prop('disabled', false);

                 var expDate = expDate + expYear;
                 var first6 = cardNumber.substr(0, 6);
                 var last4 = cardNumber.substr(12, 4);
                 var zipCode = $scope.zip_postal_code;                 
                 var data = idealsApi.pay_stored_cardReq(Cardtoken, expDate, first6, last4, cardholderName, cardType, $scope.payment, $scope.totalAmount, processType, zipCode);                              
                 
             }

             $rootScope.ShowLoader = false;
         };


         $scope.PaymentHistory = function () {
             idealsApi.payment_historyReq();
         }
     }
     catch(e)
     {
   
         $form.find('imageDiv').prop('disabled', true)

         console.log(e);
     }
}]);

   


