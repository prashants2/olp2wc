﻿angular.module('app')
    .directive('yearDrop', function ($rootScope) {
        function getYears(offset, range) {
          
            var currentDateTime = sessionStorage.currentTimeStamp;
            var currentYear = currentDateTime.substring(0, 4);         
        var years = [];
        for (var i = 0; i < range + 1; i++) {
            years.push(parseInt(currentYear) +i);
        }   
        return years;
    }
    return {
        link: function (scope, element, attrs) {
            scope.years = getYears(+attrs.offset, +attrs.range);
            scope.selected = scope.years[0];

            console.log(scope.selected);
        }
      
    }
});