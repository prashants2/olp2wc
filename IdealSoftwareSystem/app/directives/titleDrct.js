﻿angular.module('app')
    .directive('customTitle', function ($rootScope, $cookieStore) {  
        return {
            restrict: "E",
            template: '<title>' + decodeURIComponent($cookieStore.get('api_connect').title) + '</title>',
        }
    });
